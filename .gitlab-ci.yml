image: node
stages:
  - build
  - test
  - deploy review
  - deploy staging
  - deploy production
  - production tests
  - cache

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
  policy: pull

# Using workflow to specify which types of pipelines can run
# Run branch pipelines when a merge request is not open for the branch
# Run merge request pipelines when a merge request is open for the branch
workflow:
  rules:
    # If it is a merge request, run a merge request pipeline
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    # If there's a change to a branch, but a merge request is open for that branch, don't run a branch pipeline
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    # If there's a change to a branch, but without any open merge requests, run a branch pipeline
    - if: '$CI_COMMIT_BRANCH'

variables:
  STAGING_DOMAIN: https://tremendous-stem-staging.surge.sh
  PRODUCTION_DOMAIN: https://tremendous-stem.surge.sh

build website:
  stage: build
  script:
    - echo $CI_COMMIT_SHORT_SHA
    - npm install
    - npm install -g gatsby-cli
    - gatsby build
    - sed -i "s/%%VERSION%%/$CI_COMMIT_SHORT_SHA/g" ./public/index.html
  artifacts:
    paths:
      - ./public
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - when: always

test artifact:
  stage: test
  cache: []
  image: alpine
  script:
    - grep -q "Gatsby" ./public/index.html
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - when: always

test website:
  stage: test
  script:
    - npm install
    - npm install -g gatsby-cli
    - gatsby serve &
    - sleep 10
    - curl "http://localhost:9000" | grep -q "Gatsby"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - when: always

.deploy_template: &deploy_template
  script:
    - npm install --global surge
    - surge public/ $DOMAIN
  environment:
    name: $NAME
    url: $DOMAIN

deploy review:
  <<: *deploy_template
  stage: deploy review
  cache: []
  variables:
    NAME: review/$CI_COMMIT_REF_NAME
    DOMAIN: https://tremendous-stem-$CI_ENVIRONMENT_SLUG.surge.sh
    on_stop: stop review
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

stop review:
  stage: deploy review
  variables:
    # For this job, don't clone or fetch from the repository
    GIT_STRATEGY: none
  script:
    - npm install --global surge
    - surge teardown https://tremendous-stem-$CI_ENVIRONMENT_SLUG.surge.sh
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

deploy staging:
  <<: *deploy_template
  stage: deploy staging
  variables:
    DOMAIN: $STAGING_DOMAIN
    NAME: STAGING
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'

deploy production:
  <<: *deploy_template
  stage: deploy production
  variables:
    DOMAIN: $PRODUCTION_DOMAIN
    NAME: production
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'

production tests:
  stage: production tests
  cache: []
  image: alpine
  script:
    - apk add curl
    - curl $PRODUCTION_DOMAIN | grep -q "Hi peoples"
    - curl $PRODUCTION_DOMAIN | grep -q "$CI_COMMIT_SHORT_SHA"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'

update cache:
  stage: cache
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
    policy: push
  script:
    - npm install
  only:
    - schedules

# Old deploy review
.deploy review:
  stage: deploy review
  script:
    - ls
    - npm install --global surge
    - surge public/ https://tremendous-stem-$CI_ENVIRONMENT_SLUG.surge.sh
  cache: []
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://tremendous-stem-$CI_ENVIRONMENT_SLUG.surge.sh
    on_stop: stop review
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

# Old deploy staging
.deploy staging:
  stage: deploy staging
  environment:
    name: staging
    url: $STAGING_DOMAIN
  cache: []
  script:
    - npm install --global surge
    - surge public/ $STAGING_DOMAIN
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'

# Old deploy production
.deploy production:
  stage: deploy production
  environment:
    name: production
    url: $PRODUCTION_DOMAIN
  cache: []
  script:
    - npm install --global surge
    - surge public/ $PRODUCTION_DOMAIN
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "main"'
